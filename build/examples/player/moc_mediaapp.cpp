/****************************************************************************
** Meta object code from reading C++ file 'mediaapp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../examples/player/mediaapp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mediaapp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MediaApp_t {
    QByteArrayData data[11];
    char stringdata[117];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MediaApp_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MediaApp_t qt_meta_stringdata_MediaApp = {
    {
QT_MOC_LITERAL(0, 0, 8), // "MediaApp"
QT_MOC_LITERAL(1, 9, 4), // "open"
QT_MOC_LITERAL(2, 14, 0), // ""
QT_MOC_LITERAL(3, 15, 16), // "toggleFullScreen"
QT_MOC_LITERAL(4, 32, 14), // "onStateChanged"
QT_MOC_LITERAL(5, 47, 17), // "onPositionChanged"
QT_MOC_LITERAL(6, 65, 11), // "setPosition"
QT_MOC_LITERAL(7, 77, 8), // "position"
QT_MOC_LITERAL(8, 86, 12), // "showControls"
QT_MOC_LITERAL(9, 99, 4), // "show"
QT_MOC_LITERAL(10, 104, 12) // "hideControls"

    },
    "MediaApp\0open\0\0toggleFullScreen\0"
    "onStateChanged\0onPositionChanged\0"
    "setPosition\0position\0showControls\0"
    "show\0hideControls"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MediaApp[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x08 /* Private */,
       3,    0,   55,    2, 0x08 /* Private */,
       4,    0,   56,    2, 0x08 /* Private */,
       5,    0,   57,    2, 0x08 /* Private */,
       6,    1,   58,    2, 0x08 /* Private */,
       8,    1,   61,    2, 0x08 /* Private */,
       8,    0,   64,    2, 0x28 /* Private | MethodCloned */,
      10,    0,   65,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MediaApp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MediaApp *_t = static_cast<MediaApp *>(_o);
        switch (_id) {
        case 0: _t->open(); break;
        case 1: _t->toggleFullScreen(); break;
        case 2: _t->onStateChanged(); break;
        case 3: _t->onPositionChanged(); break;
        case 4: _t->setPosition((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->showControls((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->showControls(); break;
        case 7: _t->hideControls(); break;
        default: ;
        }
    }
}

const QMetaObject MediaApp::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_MediaApp.data,
      qt_meta_data_MediaApp,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MediaApp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MediaApp::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MediaApp.stringdata))
        return static_cast<void*>(const_cast< MediaApp*>(this));
    return QWidget::qt_metacast(_clname);
}

int MediaApp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

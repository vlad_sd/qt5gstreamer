# CMake generated Testfile for 
# Source directory: E:/gstreamer/qt-gstreamer/examples
# Build directory: E:/gstreamer/qt-gstreamer/build/examples
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("player")
subdirs("appsink-src")
subdirs("recorder")
subdirs("voip")
subdirs("qmlplayer")
subdirs("qmlplayer2")

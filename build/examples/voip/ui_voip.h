/********************************************************************************
** Form generated from reading UI file 'voip.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VOIP_H
#define UI_VOIP_H

#include <QGst/Ui/VideoWidget>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_VoipExample
{
public:
    QHBoxLayout *horizontalLayout_2;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label_7;
    QLineEdit *remoteHostLineEdit;
    QGroupBox *audioGroupBox;
    QGridLayout *gridLayout;
    QLabel *label;
    QSpinBox *audioRtpSrcSpinBox;
    QLabel *label_2;
    QSpinBox *audioRtcpSrcSpinBox;
    QLabel *label_3;
    QSpinBox *audioRtpDestSpinBox;
    QLabel *label_8;
    QSpinBox *audioRtcpDestSpinBox;
    QGroupBox *videoGroupBox;
    QGridLayout *gridLayout_2;
    QLabel *label_5;
    QSpinBox *videoRtpSrcSpinBox;
    QLabel *label_6;
    QSpinBox *videoRtcpSrcSpinBox;
    QLabel *label_4;
    QSpinBox *videoRtpDestSpinBox;
    QLabel *label_9;
    QSpinBox *videoRtcpDestSpinBox;
    QPushButton *startCallButton;
    QWidget *page_2;
    QVBoxLayout *verticalLayout_2;
    QGst::Ui::VideoWidget *videoWidget;
    QPushButton *endCallButton;

    void setupUi(QDialog *VoipExample)
    {
        if (VoipExample->objectName().isEmpty())
            VoipExample->setObjectName(QStringLiteral("VoipExample"));
        VoipExample->resize(400, 359);
        horizontalLayout_2 = new QHBoxLayout(VoipExample);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        stackedWidget = new QStackedWidget(VoipExample);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        verticalLayout = new QVBoxLayout(page);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_7 = new QLabel(page);
        label_7->setObjectName(QStringLiteral("label_7"));

        horizontalLayout->addWidget(label_7);

        remoteHostLineEdit = new QLineEdit(page);
        remoteHostLineEdit->setObjectName(QStringLiteral("remoteHostLineEdit"));

        horizontalLayout->addWidget(remoteHostLineEdit);


        verticalLayout->addLayout(horizontalLayout);

        audioGroupBox = new QGroupBox(page);
        audioGroupBox->setObjectName(QStringLiteral("audioGroupBox"));
        audioGroupBox->setCheckable(true);
        gridLayout = new QGridLayout(audioGroupBox);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label = new QLabel(audioGroupBox);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        audioRtpSrcSpinBox = new QSpinBox(audioGroupBox);
        audioRtpSrcSpinBox->setObjectName(QStringLiteral("audioRtpSrcSpinBox"));
        audioRtpSrcSpinBox->setMinimum(1);
        audioRtpSrcSpinBox->setMaximum(65535);
        audioRtpSrcSpinBox->setSingleStep(2);
        audioRtpSrcSpinBox->setValue(5000);

        gridLayout->addWidget(audioRtpSrcSpinBox, 0, 1, 1, 1);

        label_2 = new QLabel(audioGroupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        audioRtcpSrcSpinBox = new QSpinBox(audioGroupBox);
        audioRtcpSrcSpinBox->setObjectName(QStringLiteral("audioRtcpSrcSpinBox"));
        audioRtcpSrcSpinBox->setEnabled(false);
        audioRtcpSrcSpinBox->setMinimum(1);
        audioRtcpSrcSpinBox->setMaximum(65535);
        audioRtcpSrcSpinBox->setSingleStep(2);
        audioRtcpSrcSpinBox->setValue(5001);

        gridLayout->addWidget(audioRtcpSrcSpinBox, 1, 1, 1, 1);

        label_3 = new QLabel(audioGroupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        audioRtpDestSpinBox = new QSpinBox(audioGroupBox);
        audioRtpDestSpinBox->setObjectName(QStringLiteral("audioRtpDestSpinBox"));
        audioRtpDestSpinBox->setMinimum(1);
        audioRtpDestSpinBox->setMaximum(65535);
        audioRtpDestSpinBox->setSingleStep(2);
        audioRtpDestSpinBox->setValue(5002);

        gridLayout->addWidget(audioRtpDestSpinBox, 2, 1, 1, 1);

        label_8 = new QLabel(audioGroupBox);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout->addWidget(label_8, 3, 0, 1, 1);

        audioRtcpDestSpinBox = new QSpinBox(audioGroupBox);
        audioRtcpDestSpinBox->setObjectName(QStringLiteral("audioRtcpDestSpinBox"));
        audioRtcpDestSpinBox->setEnabled(false);
        audioRtcpDestSpinBox->setMinimum(1);
        audioRtcpDestSpinBox->setMaximum(65535);
        audioRtcpDestSpinBox->setSingleStep(2);
        audioRtcpDestSpinBox->setValue(5003);

        gridLayout->addWidget(audioRtcpDestSpinBox, 3, 1, 1, 1);


        verticalLayout->addWidget(audioGroupBox);

        videoGroupBox = new QGroupBox(page);
        videoGroupBox->setObjectName(QStringLiteral("videoGroupBox"));
        videoGroupBox->setCheckable(true);
        gridLayout_2 = new QGridLayout(videoGroupBox);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label_5 = new QLabel(videoGroupBox);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_2->addWidget(label_5, 0, 0, 1, 1);

        videoRtpSrcSpinBox = new QSpinBox(videoGroupBox);
        videoRtpSrcSpinBox->setObjectName(QStringLiteral("videoRtpSrcSpinBox"));
        videoRtpSrcSpinBox->setMinimum(1);
        videoRtpSrcSpinBox->setMaximum(65535);
        videoRtpSrcSpinBox->setSingleStep(2);
        videoRtpSrcSpinBox->setValue(5010);

        gridLayout_2->addWidget(videoRtpSrcSpinBox, 0, 1, 1, 1);

        label_6 = new QLabel(videoGroupBox);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_2->addWidget(label_6, 1, 0, 1, 1);

        videoRtcpSrcSpinBox = new QSpinBox(videoGroupBox);
        videoRtcpSrcSpinBox->setObjectName(QStringLiteral("videoRtcpSrcSpinBox"));
        videoRtcpSrcSpinBox->setEnabled(false);
        videoRtcpSrcSpinBox->setMinimum(1);
        videoRtcpSrcSpinBox->setMaximum(65535);
        videoRtcpSrcSpinBox->setSingleStep(2);
        videoRtcpSrcSpinBox->setValue(5011);

        gridLayout_2->addWidget(videoRtcpSrcSpinBox, 1, 1, 1, 1);

        label_4 = new QLabel(videoGroupBox);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_2->addWidget(label_4, 2, 0, 1, 1);

        videoRtpDestSpinBox = new QSpinBox(videoGroupBox);
        videoRtpDestSpinBox->setObjectName(QStringLiteral("videoRtpDestSpinBox"));
        videoRtpDestSpinBox->setMinimum(1);
        videoRtpDestSpinBox->setMaximum(65535);
        videoRtpDestSpinBox->setSingleStep(2);
        videoRtpDestSpinBox->setValue(5012);

        gridLayout_2->addWidget(videoRtpDestSpinBox, 2, 1, 1, 1);

        label_9 = new QLabel(videoGroupBox);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout_2->addWidget(label_9, 3, 0, 1, 1);

        videoRtcpDestSpinBox = new QSpinBox(videoGroupBox);
        videoRtcpDestSpinBox->setObjectName(QStringLiteral("videoRtcpDestSpinBox"));
        videoRtcpDestSpinBox->setEnabled(false);
        videoRtcpDestSpinBox->setMinimum(1);
        videoRtcpDestSpinBox->setMaximum(65535);
        videoRtcpDestSpinBox->setSingleStep(2);
        videoRtcpDestSpinBox->setValue(5013);

        gridLayout_2->addWidget(videoRtcpDestSpinBox, 3, 1, 1, 1);


        verticalLayout->addWidget(videoGroupBox);

        startCallButton = new QPushButton(page);
        startCallButton->setObjectName(QStringLiteral("startCallButton"));

        verticalLayout->addWidget(startCallButton);

        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QStringLiteral("page_2"));
        verticalLayout_2 = new QVBoxLayout(page_2);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        videoWidget = new QGst::Ui::VideoWidget(page_2);
        videoWidget->setObjectName(QStringLiteral("videoWidget"));

        verticalLayout_2->addWidget(videoWidget);

        endCallButton = new QPushButton(page_2);
        endCallButton->setObjectName(QStringLiteral("endCallButton"));

        verticalLayout_2->addWidget(endCallButton);

        stackedWidget->addWidget(page_2);

        horizontalLayout_2->addWidget(stackedWidget);


        retranslateUi(VoipExample);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(VoipExample);
    } // setupUi

    void retranslateUi(QDialog *VoipExample)
    {
        VoipExample->setWindowTitle(QApplication::translate("VoipExample", "QtGStreamer VoIP example", 0));
        label_7->setText(QApplication::translate("VoipExample", "Remote host", 0));
        remoteHostLineEdit->setText(QApplication::translate("VoipExample", "127.0.0.1", 0));
        audioGroupBox->setTitle(QApplication::translate("VoipExample", "Audio stream", 0));
        label->setText(QApplication::translate("VoipExample", "RTP source port", 0));
        label_2->setText(QApplication::translate("VoipExample", "RTCP source port", 0));
        label_3->setText(QApplication::translate("VoipExample", "RTP destination port", 0));
        label_8->setText(QApplication::translate("VoipExample", "RTCP destination port", 0));
        videoGroupBox->setTitle(QApplication::translate("VoipExample", "Video stream", 0));
        label_5->setText(QApplication::translate("VoipExample", "RTP source port", 0));
        label_6->setText(QApplication::translate("VoipExample", "RTCP source port", 0));
        label_4->setText(QApplication::translate("VoipExample", "RTP destination port", 0));
        label_9->setText(QApplication::translate("VoipExample", "RTCP destination port", 0));
        startCallButton->setText(QApplication::translate("VoipExample", "Start Call", 0));
        endCallButton->setText(QApplication::translate("VoipExample", "End Call", 0));
    } // retranslateUi

};

namespace Ui {
    class VoipExample: public Ui_VoipExample {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VOIP_H

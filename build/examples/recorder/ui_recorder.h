/********************************************************************************
** Form generated from reading UI file 'recorder.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RECORDER_H
#define UI_RECORDER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Recorder
{
public:
    QGridLayout *gridLayout;
    QGroupBox *audioSourceGroupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *audioSourceLabel;
    QComboBox *audioSourceComboBox;
    QGroupBox *videoSourceGroupBox;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_4;
    QLabel *videoSourceLabel;
    QComboBox *videoSourceComboBox;
    QStackedWidget *videoSourcePropertiesWidget;
    QWidget *cameraPage;
    QHBoxLayout *horizontalLayout_5;
    QLabel *emptyLabel;
    QWidget *screencastPage;
    QHBoxLayout *horizontalLayout_6;
    QLabel *displayNumLabel;
    QSpinBox *displayNumSpinBox;
    QHBoxLayout *horizontalLayout;
    QLabel *outputFileLabel;
    QLineEdit *outputFileEdit;
    QSpacerItem *horizontalSpacer;
    QPushButton *startStopButton;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QDialog *Recorder)
    {
        if (Recorder->objectName().isEmpty())
            Recorder->setObjectName(QStringLiteral("Recorder"));
        Recorder->resize(306, 291);
        gridLayout = new QGridLayout(Recorder);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        audioSourceGroupBox = new QGroupBox(Recorder);
        audioSourceGroupBox->setObjectName(QStringLiteral("audioSourceGroupBox"));
        verticalLayout = new QVBoxLayout(audioSourceGroupBox);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        audioSourceLabel = new QLabel(audioSourceGroupBox);
        audioSourceLabel->setObjectName(QStringLiteral("audioSourceLabel"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(audioSourceLabel->sizePolicy().hasHeightForWidth());
        audioSourceLabel->setSizePolicy(sizePolicy);

        horizontalLayout_3->addWidget(audioSourceLabel);

        audioSourceComboBox = new QComboBox(audioSourceGroupBox);
        audioSourceComboBox->setObjectName(QStringLiteral("audioSourceComboBox"));

        horizontalLayout_3->addWidget(audioSourceComboBox);


        verticalLayout->addLayout(horizontalLayout_3);


        gridLayout->addWidget(audioSourceGroupBox, 0, 0, 1, 3);

        videoSourceGroupBox = new QGroupBox(Recorder);
        videoSourceGroupBox->setObjectName(QStringLiteral("videoSourceGroupBox"));
        verticalLayout_2 = new QVBoxLayout(videoSourceGroupBox);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        videoSourceLabel = new QLabel(videoSourceGroupBox);
        videoSourceLabel->setObjectName(QStringLiteral("videoSourceLabel"));
        sizePolicy.setHeightForWidth(videoSourceLabel->sizePolicy().hasHeightForWidth());
        videoSourceLabel->setSizePolicy(sizePolicy);

        horizontalLayout_4->addWidget(videoSourceLabel);

        videoSourceComboBox = new QComboBox(videoSourceGroupBox);
        videoSourceComboBox->setObjectName(QStringLiteral("videoSourceComboBox"));

        horizontalLayout_4->addWidget(videoSourceComboBox);


        verticalLayout_2->addLayout(horizontalLayout_4);

        videoSourcePropertiesWidget = new QStackedWidget(videoSourceGroupBox);
        videoSourcePropertiesWidget->setObjectName(QStringLiteral("videoSourcePropertiesWidget"));
        cameraPage = new QWidget();
        cameraPage->setObjectName(QStringLiteral("cameraPage"));
        horizontalLayout_5 = new QHBoxLayout(cameraPage);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        emptyLabel = new QLabel(cameraPage);
        emptyLabel->setObjectName(QStringLiteral("emptyLabel"));
        sizePolicy.setHeightForWidth(emptyLabel->sizePolicy().hasHeightForWidth());
        emptyLabel->setSizePolicy(sizePolicy);

        horizontalLayout_5->addWidget(emptyLabel);

        videoSourcePropertiesWidget->addWidget(cameraPage);
        screencastPage = new QWidget();
        screencastPage->setObjectName(QStringLiteral("screencastPage"));
        horizontalLayout_6 = new QHBoxLayout(screencastPage);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        displayNumLabel = new QLabel(screencastPage);
        displayNumLabel->setObjectName(QStringLiteral("displayNumLabel"));
        sizePolicy.setHeightForWidth(displayNumLabel->sizePolicy().hasHeightForWidth());
        displayNumLabel->setSizePolicy(sizePolicy);

        horizontalLayout_6->addWidget(displayNumLabel);

        displayNumSpinBox = new QSpinBox(screencastPage);
        displayNumSpinBox->setObjectName(QStringLiteral("displayNumSpinBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(displayNumSpinBox->sizePolicy().hasHeightForWidth());
        displayNumSpinBox->setSizePolicy(sizePolicy1);

        horizontalLayout_6->addWidget(displayNumSpinBox);

        videoSourcePropertiesWidget->addWidget(screencastPage);

        verticalLayout_2->addWidget(videoSourcePropertiesWidget);


        gridLayout->addWidget(videoSourceGroupBox, 1, 0, 1, 3);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        outputFileLabel = new QLabel(Recorder);
        outputFileLabel->setObjectName(QStringLiteral("outputFileLabel"));

        horizontalLayout->addWidget(outputFileLabel);

        outputFileEdit = new QLineEdit(Recorder);
        outputFileEdit->setObjectName(QStringLiteral("outputFileEdit"));

        horizontalLayout->addWidget(outputFileEdit);


        gridLayout->addLayout(horizontalLayout, 2, 0, 1, 3);

        horizontalSpacer = new QSpacerItem(76, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 3, 0, 1, 1);

        startStopButton = new QPushButton(Recorder);
        startStopButton->setObjectName(QStringLiteral("startStopButton"));

        gridLayout->addWidget(startStopButton, 3, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(87, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 3, 2, 1, 1);


        retranslateUi(Recorder);
        QObject::connect(videoSourceComboBox, SIGNAL(currentIndexChanged(int)), videoSourcePropertiesWidget, SLOT(setCurrentIndex(int)));

        videoSourcePropertiesWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Recorder);
    } // setupUi

    void retranslateUi(QDialog *Recorder)
    {
        Recorder->setWindowTitle(QApplication::translate("Recorder", "QtGStreamer recorder example", 0));
        audioSourceGroupBox->setTitle(QApplication::translate("Recorder", "Audio source", 0));
        audioSourceLabel->setText(QApplication::translate("Recorder", "Audio source:", 0));
        audioSourceComboBox->clear();
        audioSourceComboBox->insertItems(0, QStringList()
         << QApplication::translate("Recorder", "Microphone", 0)
        );
        videoSourceGroupBox->setTitle(QApplication::translate("Recorder", "Video source", 0));
        videoSourceLabel->setText(QApplication::translate("Recorder", "Video source:", 0));
        videoSourceComboBox->clear();
        videoSourceComboBox->insertItems(0, QStringList()
         << QApplication::translate("Recorder", "Camera", 0)
         << QApplication::translate("Recorder", "Screencast (X11)", 0)
        );
        displayNumLabel->setText(QApplication::translate("Recorder", "Display number:", 0));
        outputFileLabel->setText(QApplication::translate("Recorder", "Output file (ogg):", 0));
        startStopButton->setText(QApplication::translate("Recorder", "Start recording", 0));
    } // retranslateUi

};

namespace Ui {
    class Recorder: public Ui_Recorder {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RECORDER_H

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "E:/gstreamer/qt-gstreamer/build/examples/appsink-src/appsink-src_automoc.cpp" "E:/gstreamer/qt-gstreamer/build/examples/appsink-src/CMakeFiles/appsink-src.dir/appsink-src_automoc.cpp.obj"
  "E:/gstreamer/qt-gstreamer/examples/appsink-src/main.cpp" "E:/gstreamer/qt-gstreamer/build/examples/appsink-src/CMakeFiles/appsink-src.dir/main.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_NO_DEBUG"
  "QT_NO_KEYWORDS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "examples/appsink-src"
  "../examples/appsink-src"
  "../src"
  "E:/gstreamer/boost_1_59_0"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtCore"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/mkspecs/win32-g++"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "E:/gstreamer/qt-gstreamer/build/src/QGst/CMakeFiles/Qt5GStreamerUtils.dir/DependInfo.cmake"
  "E:/gstreamer/qt-gstreamer/build/src/QGst/CMakeFiles/Qt5GStreamer.dir/DependInfo.cmake"
  "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

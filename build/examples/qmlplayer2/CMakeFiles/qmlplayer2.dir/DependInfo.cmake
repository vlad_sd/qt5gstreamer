# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "E:/gstreamer/qt-gstreamer/examples/qmlplayer2/main.cpp" "E:/gstreamer/qt-gstreamer/build/examples/qmlplayer2/CMakeFiles/qmlplayer2.dir/main.cpp.obj"
  "E:/gstreamer/qt-gstreamer/examples/qmlplayer2/player.cpp" "E:/gstreamer/qt-gstreamer/build/examples/qmlplayer2/CMakeFiles/qmlplayer2.dir/player.cpp.obj"
  "E:/gstreamer/qt-gstreamer/build/examples/qmlplayer2/qmlplayer2_automoc.cpp" "E:/gstreamer/qt-gstreamer/build/examples/qmlplayer2/CMakeFiles/qmlplayer2.dir/qmlplayer2_automoc.cpp.obj"
  "E:/gstreamer/qt-gstreamer/build/examples/qmlplayer2/qrc_qmlplayer2.cpp" "E:/gstreamer/qt-gstreamer/build/examples/qmlplayer2/CMakeFiles/qmlplayer2.dir/qrc_qmlplayer2.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QTVIDEOSINK_PATH=\"E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink\""
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NETWORK_LIB"
  "QT_NO_DEBUG"
  "QT_NO_KEYWORDS"
  "QT_QML_LIB"
  "QT_QUICK_LIB"
  "UNINSTALLED_IMPORTS_DIR=\"E:/gstreamer/qt-gstreamer/build/src/qml/quick2\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "examples/qmlplayer2"
  "../examples/qmlplayer2"
  "../src"
  "E:/gstreamer/boost_1_59_0"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtCore"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/mkspecs/win32-g++"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtGui"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtQuick"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtQml"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtNetwork"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "E:/gstreamer/qt-gstreamer/build/src/QGst/CMakeFiles/Qt5GStreamerQuick.dir/DependInfo.cmake"
  "E:/gstreamer/qt-gstreamer/build/src/QGst/CMakeFiles/Qt5GStreamer.dir/DependInfo.cmake"
  "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

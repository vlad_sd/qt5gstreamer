# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "E:/gstreamer/qt-gstreamer/build/src/qml/quick1/QtGStreamerQuick1_automoc.cpp" "E:/gstreamer/qt-gstreamer/build/src/qml/quick1/CMakeFiles/QtGStreamerQuick1.dir/QtGStreamerQuick1_automoc.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/qml/quick1/plugin.cpp" "E:/gstreamer/qt-gstreamer/build/src/qml/quick1/CMakeFiles/QtGStreamerQuick1.dir/plugin.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/qml/quick1/videoitem.cpp" "E:/gstreamer/qt-gstreamer/build/src/qml/quick1/CMakeFiles/QtGStreamerQuick1.dir/videoitem.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QTGLVIDEOSINK_NAME=\"qt5glvideosink\""
  "QTVIDEOSINK_NAME=\"qt5videosink\""
  "QT_CORE_LIB"
  "QT_DECLARATIVE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "QT_NO_KEYWORDS"
  "QT_SCRIPT_LIB"
  "QT_WIDGETS_LIB"
  "QWIDGETVIDEOSINK_NAME=\"qwidget5videosink\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/qml/quick1"
  "../src/qml/quick1"
  "../src"
  "E:/gstreamer/boost_1_59_0"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtCore"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/mkspecs/win32-g++"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtDeclarative"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtWidgets"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtGui"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtScript"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "E:/gstreamer/qt-gstreamer/build/src/QGst/CMakeFiles/Qt5GStreamerUi.dir/DependInfo.cmake"
  "E:/gstreamer/qt-gstreamer/build/src/QGst/CMakeFiles/Qt5GStreamer.dir/DependInfo.cmake"
  "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

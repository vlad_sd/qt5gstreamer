# Install script for directory: E:/gstreamer/qt-gstreamer/src/QGst

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/QtGStreamer")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/gstreamer/qt-gstreamer/build/src/QGst/libQt5GStreamer-1.0.dll.a")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "E:/gstreamer/qt-gstreamer/build/src/QGst/libQt5GStreamer-1.0.dll")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/libQt5GStreamer-1.0.dll" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/libQt5GStreamer-1.0.dll")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "E:/Qt/Qt5.4.1/Tools/mingw491_32/bin/strip.exe" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/libQt5GStreamer-1.0.dll")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/gstreamer/qt-gstreamer/build/src/QGst/libQt5GStreamerQuick-1.0.dll.a")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "E:/gstreamer/qt-gstreamer/build/src/QGst/libQt5GStreamerQuick-1.0.dll")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/libQt5GStreamerQuick-1.0.dll" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/libQt5GStreamerQuick-1.0.dll")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "E:/Qt/Qt5.4.1/Tools/mingw491_32/bin/strip.exe" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/libQt5GStreamerQuick-1.0.dll")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/gstreamer/qt-gstreamer/build/src/QGst/libQt5GStreamerUi-1.0.dll.a")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "E:/gstreamer/qt-gstreamer/build/src/QGst/libQt5GStreamerUi-1.0.dll")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/libQt5GStreamerUi-1.0.dll" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/libQt5GStreamerUi-1.0.dll")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "E:/Qt/Qt5.4.1/Tools/mingw491_32/bin/strip.exe" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/libQt5GStreamerUi-1.0.dll")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/gstreamer/qt-gstreamer/build/src/QGst/libQt5GStreamerUtils-1.0.dll.a")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "E:/gstreamer/qt-gstreamer/build/src/QGst/libQt5GStreamerUtils-1.0.dll")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/libQt5GStreamerUtils-1.0.dll" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/libQt5GStreamerUtils-1.0.dll")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "E:/Qt/Qt5.4.1/Tools/mingw491_32/bin/strip.exe" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/libQt5GStreamerUtils-1.0.dll")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/global.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Global")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/init.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Init")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/enums.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/structs.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Fourcc")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Fraction")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/IntRange")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Int64Range")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/DoubleRange")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/FractionRange")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/structure.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Structure")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/caps.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Caps")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/miniobject.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/MiniObject")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/object.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Object")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/pad.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Pad")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/ghostpad.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/GhostPad")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/element.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Element")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/pluginfeature.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/PluginFeature")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/elementfactory.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/ElementFactory")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/bin.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Bin")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/childproxy.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/ChildProxy")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/pipeline.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Pipeline")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/message.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Message")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/bus.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Bus")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/parse.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Parse")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/urihandler.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/UriHandler")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/videoorientation.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/VideoOrientation")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/videooverlay.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/VideoOverlay")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/streamvolume.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/StreamVolume")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/colorbalance.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/ColorBalance")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/query.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Query")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/clock.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Clock")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/buffer.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Buffer")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/sample.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Sample")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/allocator.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Allocator")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/memory.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Memory")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/event.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Event")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/clocktime.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/ClockTime")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/taglist.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/TagList")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/bufferlist.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/BufferList")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/discoverer.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Discoverer")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/segment.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Segment")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Ui" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Ui/global.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Ui" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Ui/videowidget.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Ui" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Ui/VideoWidget")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Ui" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Ui/graphicsvideosurface.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Ui" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Ui/GraphicsVideoSurface")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Ui" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Ui/graphicsvideowidget.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Ui" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Ui/GraphicsVideoWidget")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Utils" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Utils/global.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Utils" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Utils/applicationsink.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Utils" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Utils/ApplicationSink")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Utils" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Utils/applicationsource.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Utils" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Utils/ApplicationSource")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Quick" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Quick/global.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Quick" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Quick/videosurface.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Quick" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Quick/VideoSurface")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Quick" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Quick/videoitem.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Qt5GStreamer/QGst/Quick" TYPE FILE FILES "E:/gstreamer/qt-gstreamer/src/QGst/Quick/VideoItem")
endif()


# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "E:/gstreamer/qt-gstreamer/build/src/QGst/Qt5GStreamerUtils_automoc.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGst/CMakeFiles/Qt5GStreamerUtils.dir/Qt5GStreamerUtils_automoc.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/QGst/Utils/applicationsink.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGst/CMakeFiles/Qt5GStreamerUtils.dir/Utils/applicationsink.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/QGst/Utils/applicationsource.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGst/CMakeFiles/Qt5GStreamerUtils.dir/Utils/applicationsource.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GST_DISABLE_LOADSAVE"
  "GST_DISABLE_XML"
  "QTGLVIDEOSINK_NAME=\"qt5glvideosink\""
  "QTVIDEOSINK_NAME=\"qt5videosink\""
  "QT_CORE_LIB"
  "QT_NO_DEBUG"
  "QT_NO_KEYWORDS"
  "QWIDGETVIDEOSINK_NAME=\"qwidget5videosink\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/QGst"
  "../src/QGst"
  "../src"
  "E:/gstreamer/boost_1_59_0"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtCore"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/mkspecs/win32-g++"
  "E:/gstreamer/1.0/x86/include/gstreamer-1.0"
  "E:/gstreamer/1.0/x86/include/glib-2.0"
  "E:/gstreamer/1.0/x86/include/../lib/glib-2.0/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "E:/gstreamer/qt-gstreamer/build/src/QGst/CMakeFiles/Qt5GStreamer.dir/DependInfo.cmake"
  "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

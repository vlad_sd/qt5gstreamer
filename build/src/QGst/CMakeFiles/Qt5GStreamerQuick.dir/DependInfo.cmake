# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "E:/gstreamer/qt-gstreamer/build/src/QGst/Qt5GStreamerQuick_automoc.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGst/CMakeFiles/Qt5GStreamerQuick.dir/Qt5GStreamerQuick_automoc.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/QGst/Quick/videoitem.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGst/CMakeFiles/Qt5GStreamerQuick.dir/Quick/videoitem.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/QGst/Quick/videosurface.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGst/CMakeFiles/Qt5GStreamerQuick.dir/Quick/videosurface.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GST_DISABLE_LOADSAVE"
  "GST_DISABLE_XML"
  "QTGLVIDEOSINK_NAME=\"qt5glvideosink\""
  "QTVIDEOSINK_NAME=\"qt5videosink\""
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NETWORK_LIB"
  "QT_NO_DEBUG"
  "QT_NO_KEYWORDS"
  "QT_QML_LIB"
  "QT_QUICK_LIB"
  "QWIDGETVIDEOSINK_NAME=\"qwidget5videosink\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/QGst"
  "../src/QGst"
  "../src"
  "E:/gstreamer/boost_1_59_0"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtCore"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/mkspecs/win32-g++"
  "E:/gstreamer/1.0/x86/include/gstreamer-1.0"
  "E:/gstreamer/1.0/x86/include/glib-2.0"
  "E:/gstreamer/1.0/x86/include/../lib/glib-2.0/include"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtQuick"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtGui"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtQml"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtNetwork"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "E:/gstreamer/qt-gstreamer/build/src/QGst/CMakeFiles/Qt5GStreamer.dir/DependInfo.cmake"
  "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

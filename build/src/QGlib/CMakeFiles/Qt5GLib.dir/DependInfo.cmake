# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "E:/gstreamer/qt-gstreamer/build/src/QGlib/Qt5GLib_automoc.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/Qt5GLib_automoc.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/QGlib/connect.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/connect.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/QGlib/error.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/error.cpp.obj"
  "E:/gstreamer/qt-gstreamer/build/src/QGlib/gen.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/gen.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/QGlib/init.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/init.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/QGlib/object.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/object.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/QGlib/paramspec.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/paramspec.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/QGlib/quark.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/quark.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/QGlib/signal.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/signal.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/QGlib/type.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/type.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/QGlib/value.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/value.cpp.obj"
  "E:/gstreamer/qt-gstreamer/src/QGlib/wrap.cpp" "E:/gstreamer/qt-gstreamer/build/src/QGlib/CMakeFiles/Qt5GLib.dir/wrap.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QTGLVIDEOSINK_NAME=\"qt5glvideosink\""
  "QTVIDEOSINK_NAME=\"qt5videosink\""
  "QT_CORE_LIB"
  "QT_NO_DEBUG"
  "QT_NO_KEYWORDS"
  "QWIDGETVIDEOSINK_NAME=\"qwidget5videosink\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/QGlib"
  "../src/QGlib"
  "../src"
  "E:/gstreamer/boost_1_59_0"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtCore"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/mkspecs/win32-g++"
  "E:/gstreamer/1.0/x86/include/glib-2.0"
  "E:/gstreamer/1.0/x86/include/../lib/glib-2.0/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/gstqtvideosinkmarshal.c" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/gstqtvideosinkmarshal.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "GST_DISABLE_LOADSAVE"
  "GST_DISABLE_XML"
  "PACKAGE=\"qt-gstreamer\""
  "PACKAGE_NAME=\"QtGStreamer\""
  "PACKAGE_ORIGIN=\"http://gstreamer.freedesktop.org/\""
  "PACKAGE_VERSION=\"1.2.0\""
  "QTGLVIDEOSINK_NAME=qt5glvideosink"
  "QTVIDEOSINK_NAME=qt5videosink"
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NETWORK_LIB"
  "QT_NO_DEBUG"
  "QT_OPENGL_LIB"
  "QT_QML_LIB"
  "QT_QUICK_LIB"
  "QT_WIDGETS_LIB"
  "QWIDGETVIDEOSINK_NAME=qwidget5videosink"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "elements/gstqtvideosink"
  "../elements/gstqtvideosink"
  "E:/gstreamer/1.0/x86/include/gstreamer-1.0"
  "E:/gstreamer/1.0/x86/include/glib-2.0"
  "E:/gstreamer/1.0/x86/include/../lib/glib-2.0/include"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtCore"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/mkspecs/win32-g++"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtGui"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtWidgets"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtQuick"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtQml"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtNetwork"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtOpenGL"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/delegates/basedelegate.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/delegates/basedelegate.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/delegates/qtquick2videosinkdelegate.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/delegates/qtquick2videosinkdelegate.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/delegates/qtvideosinkdelegate.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/delegates/qtvideosinkdelegate.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/delegates/qwidgetvideosinkdelegate.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/delegates/qwidgetvideosinkdelegate.cpp.obj"
  "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/gstqt5videosink_automoc.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/gstqt5videosink_automoc.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/gstqtglvideosink.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/gstqtglvideosink.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/gstqtglvideosinkbase.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/gstqtglvideosinkbase.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/gstqtquick2videosink.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/gstqtquick2videosink.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/gstqtvideosink.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/gstqtvideosink.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/gstqtvideosinkbase.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/gstqtvideosinkbase.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/gstqtvideosinkplugin.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/gstqtvideosinkplugin.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/gstqwidgetvideosink.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/gstqwidgetvideosink.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/painters/genericsurfacepainter.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/painters/genericsurfacepainter.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/painters/openglsurfacepainter.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/painters/openglsurfacepainter.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/painters/videomaterial.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/painters/videomaterial.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/painters/videonode.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/painters/videonode.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/utils/bufferformat.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/utils/bufferformat.cpp.obj"
  "E:/gstreamer/qt-gstreamer/elements/gstqtvideosink/utils/utils.cpp" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/CMakeFiles/gstqt5videosink.dir/utils/utils.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GST_DISABLE_LOADSAVE"
  "GST_DISABLE_XML"
  "PACKAGE=\"qt-gstreamer\""
  "PACKAGE_NAME=\"QtGStreamer\""
  "PACKAGE_ORIGIN=\"http://gstreamer.freedesktop.org/\""
  "PACKAGE_VERSION=\"1.2.0\""
  "QTGLVIDEOSINK_NAME=qt5glvideosink"
  "QTVIDEOSINK_NAME=qt5videosink"
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NETWORK_LIB"
  "QT_NO_DEBUG"
  "QT_OPENGL_LIB"
  "QT_QML_LIB"
  "QT_QUICK_LIB"
  "QT_WIDGETS_LIB"
  "QWIDGETVIDEOSINK_NAME=qwidget5videosink"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "elements/gstqtvideosink"
  "../elements/gstqtvideosink"
  "E:/gstreamer/1.0/x86/include/gstreamer-1.0"
  "E:/gstreamer/1.0/x86/include/glib-2.0"
  "E:/gstreamer/1.0/x86/include/../lib/glib-2.0/include"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtCore"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/mkspecs/win32-g++"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtGui"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtWidgets"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtQuick"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtQml"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtNetwork"
  "E:/Qt/Qt5.4.1/5.4/mingw491_32/include/QtOpenGL"
  )

# Pairs of files generated by the same build rule.
set(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/gstqtvideosinkmarshal.c" "E:/gstreamer/qt-gstreamer/build/elements/gstqtvideosink/gstqtvideosinkmarshal.h"
  )


# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
